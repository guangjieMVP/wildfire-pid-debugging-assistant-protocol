### 使用步骤

#### 1、初始化协议

```c
int32_t protocol_init(void);
```

#### 2、接收协议数据

```c
void protocol_data_recv(uint8_t *data, uint16_t data_len);
```

#### 3、主函数中循环处理协议

```c
int8_t receiving_process(void);
```

#### 4、发送数据给上位机

```c
void set_computer_value(uint8_t cmd, uint8_t ch, void *data, uint8_t num);
```

#### 5、实现__weak修饰的函数

```c
/**
  * @brief  最终发送数据输出的底层接口函数，需要用户实现
  * @param data : 发送的数据
  * @param num: 参数大小
  * @retval none
  */
__weak void port_send_data_to_computer(void *data, uint8_t num);

/**
  * @brief  处理上位机发送过来的PID参数
  * @param  p : PID p参数
  * @param  i: PID i参数
  * @param  d : PID d参数
  * @retval none
  * @note  需要用户实现
  */
__weak void set_pid_paramter_cmd(float p, float i, float d);

/**
 * @brief:  处理上位机开始PID控制命令
 * @param:  none
 * @retval: none
 * @note:   需要用户实现
 */
__weak void pid_start_cmd(void);


/**
 * @brief:  处理上位机停止PID控制命令
 * @param:  none
 * @retval: none
 * @note:   需要用户实现
 */
__weak void pid_stop_cmd(void);


/**
 * @brief:  处理上位机复位命令
 * @param:  none
 * @retval: none
 * @note:   需要用户实现
 */
__weak void pid_reset_cmd(void);

/**
 * @brief:  处理上位机设置目标值命令
 * @param:  actual_val - 目标值
 * @retval: none
 * @note:   需要用户实现
 */
__weak void set_pid_actual_val_cmd(int actual_val);

/**
 * @brief: 设置PID周期数  
 * @param: period - 周期数
 * @retval:  
 * @note:  需要用户实现
 */
__weak void set_pid_period_cmd(uint32_t period);
```

